class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name

  has_many :owned_systems, :foreign_key=>'owner_id', :class_name => 'System'

  has_many :access_requests, :foreign_key=>'user_id', :class_name => 'Access'
  has_many :athoriser_requests, :foreign_key=>'authorised_by', :class_name => 'Access'
  has_many :actioner_requests, :foreign_key=>'actioned_by', :class_name=>'Access'

  has_many :subordinates, :foreign_key=>'line_manager', :class_name=>'User'
  has_one :line_manager, :foreign_key => 'id', :class_name=>'User'

end
