class System < ActiveRecord::Base
	paginates_per 5
	has_many :access_levels
	has_many :accesses
	belongs_to :owner , :foreign_key => 'owner_id' , :class_name => 'User'
	belongs_to :admin1 , :foreign_key => 'admin1_id', :class_name => 'User'
	belongs_to :admin2 , :foreign_key => 'admin2_id', :class_name => 'User'

	accepts_nested_attributes_for :access_levels, :allow_destroy => true, :reject_if => :reject_al_destroy

	def reject_al_destroy(attributed)
		if attributed['_destroy'] == 1
			Access.where(:level_id => attributed['id'])
			return true
		else
			return false
		end
	end
end
