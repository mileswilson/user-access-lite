class Access < ActiveRecord::Base
		paginates_per 5
		belongs_to :user
		belongs_to :authoriser, :foreign_key => 'authorised_by', :class_name => 'User'
		belongs_to :actioner, :primary_key => 'actioned_by', :class_name => 'User'
		belongs_to :system
		has_one :access_leve, :primary_key => 'level_id', :class_name => 'AccessLevel'
end
