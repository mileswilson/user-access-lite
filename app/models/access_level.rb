class AccessLevel < ActiveRecord::Base
  belongs_to :system
  belongs_to :access, :foreign_key => 'level_id', :class_name => 'Access'
end
