class UserControlController < ApplicationController

def index
@users = User.page(params[:page])
   respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
end

# GET /systems/1
  # GET /systems/1.json
  def show
    @user = User.find(params[:id])
    @accesses = Access.where(:user_id => @user.id)
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @system }
    end
  end

end
