class AccessesController < ApplicationController
  autocomplete :user, :name
  autocomplete :system, :name
  # GET /accesses
  # GET /accesses.json
  def index
    @accesses = Access.page(params[:page])
    systems = System.where(:owner_id => current_user.id )
    action_systems = System.where("admin1_id = ? OR admin2_id = ?", current_user.id,current_user.id)
    @authorise_me = Access.where("system_id IN (?) AND authorised_by IS NULL",systems).page(params[:page])
    @action_me = Access.where("system_id IN (?) AND authorised_by IS NOT NULL AND actioned_by IS NULL", action_systems).page(params[:page])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @accesses }
    end
  end

  # GET /accesses/1
  # GET /accesses/1.json
  def show
    @access = Access.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @access }
    end
  end

  # GET /accesses/new
  # GET /accesses/new.json
  def new
    @access = Access.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @access }
    end
  end

  # GET /accesses/1/edit
  def edit
    @access = Access.find(params[:id])
  end

  def authorise
    @access = Access.find(params[:id])
    if @access.system.owner_id == current_user.id
      @access.authorised_by = current_user
      @access.authorised_date =  Date.current

    else
      @access.errors.add "You're not the system owner - gerrof!"
    end

    respond_to do |format|
      if @access.save
        format.html { redirect_to @access, notice: 'Access was successfully created.' }
        format.json { render json: @access, status: :created, location: @access }
      else
        format.html { render action: "new" }
        format.json { render json: @access.errors, status: :unprocessable_entity }
      end
    end
  end

  def action
    @access = Access.find(params[:id])
    if request.get?
      
      render partial:  'action_request' , :locals => { :ac => @access}
    end
    if request.put?
      if @access.update_attributes(params[:access])
        @access.actioned_by = current_user.id
        @access.actioned_date = Date.current
        @access.save()
      end
      if 
        ready = { :ready => true }
      else
        ready = { :ready => false }
      end
      render json: ready
    end
  end

  # POST /accesses
  # POST /accesses.json
  def create
    @access = Access.new(params[:access])

    respond_to do |format|
      if @access.save
        format.html { redirect_to @access, notice: 'Access was successfully created.' }
        format.json { render json: @access, status: :created, location: @access }
      else
        format.html { render action: "new" }
        format.json { render json: @access.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /accesses/1
  # PUT /accesses/1.json
  def update
    @access = Access.find(params[:id])

    respond_to do |format|
      if @access.update_attributes(params[:access])
        format.html { redirect_to @access, notice: 'Access was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @access.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accesses/1
  # DELETE /accesses/1.json
  def destroy
    @access = Access.find(params[:id])
    @access.destroy

    respond_to do |format|
      format.html { redirect_to accesses_url }
      format.json { head :ok }
    end
  end
end
