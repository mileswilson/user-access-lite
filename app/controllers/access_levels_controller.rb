class AccessLevelsController < ApplicationController
	def create
		@system = System.find(params[:system_id])
		@access_level = @system.access_levels.create(params[:access_level])
		redirect_to system_path(@system)
	end

	def destroy
		@al = AccessLevel.find(params[:id])
		inuse = Access.where(:level_id => params[:id])
		if inuse.empty?
			@al.destroy
		else
			@al.errors.add 'Access Level in use'
		end
	end

end
