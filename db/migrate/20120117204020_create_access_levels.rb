class CreateAccessLevels < ActiveRecord::Migration
  def change
    create_table :access_levels do |t|
      t.string :name
      t.references :system

      t.timestamps
    end
    add_index :access_levels, :system_id
  end
end
