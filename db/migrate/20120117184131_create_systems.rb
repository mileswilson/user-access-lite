class CreateSystems < ActiveRecord::Migration
  def change
    create_table :systems do |t|
      t.string :name
      t.text :description
      t.integer :owner_id
      t.integer :admin1_id
      t.integer :admin2_id

      t.timestamps
    end
  end
end
