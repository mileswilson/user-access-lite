class CreateAccesses < ActiveRecord::Migration
  def change
    create_table :accesses do |t|
      t.integer :user_id
      t.integer :level_id
      t.date :request_date
      t.integer :authorised_by
      t.date :authorised_date
      t.integer :actioned_by
      t.date :actioned_date
      t.string :username
      t.string :temp_password

      t.timestamps
    end
  end
end
