class AddReasonToAccesses < ActiveRecord::Migration
  def change
    add_column :accesses, :reason, :text
  end
end
